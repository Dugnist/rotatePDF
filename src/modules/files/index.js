const fs = require('fs');
const { routes } = require('./config.json');

module.exports = ({ ACTIONS, ROUTER, utils }) => {

  /**
   *****************************************
   * GET CORRECT ACTIONS NAMES FROM CONFIG *
   *****************************************
   */

  const { files_upload, files_download } = utils.convertkeysToDots(routes);

  /**
   *************************************
   * ADD files ROUTES TO ACTIONS TREAD *
   *************************************
   */

  ROUTER.set('routes', routes);

  /**
   *****************************
   * SUBSCRIBE TO FILES UPLOAD *
   *****************************
   *
   * @param  {object} headers - http headers
   * @param  {object} query - parameters from the url
   * @param  {object} body - parameters from json body
   * @return {promise} - success response or error
   */
  ACTIONS.on(files_upload, ({ headers, query, body, path }) => {

    return new Promise((resolve, reject) => {

      fs.readFile(path, (err, data) => {

        if (err) throw err;

        ACTIONS.send('pdftk.create', { length: body.length, file: data })
          .then((id) => (resolve({ fileId: id })))
          .catch((error) => reject({ code: 300, details: error.message }));

      });

    });

  });

  /**
   *******************************
   * SUBSCRIBE TO FILES DOWNLOAD *
   *******************************
   *
   * @param  {object} headers - http headers
   * @param  {object} query - parameters from the url
   * @param  {object} body - parameters from json body
   * @return {promise} - success response or error
   */
  ACTIONS.on(files_download, ({ headers, query, body }) => {

    console.log({ id: query.id, pages: JSON.parse(query.pages) });

    return ACTIONS.send('pdftk.handle', { id: query.id, pages: query.pages })
      .then((pathFile) =>
        Promise.resolve({ type: 'file', path: pathFile }))
      .catch((error) =>
        Promise.reject({ code: error.code, details: error.message }));

  });

};
