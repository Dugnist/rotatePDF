const CONFIG = require('config');
const API = require(`./${CONFIG.framework}_api/index`);
const PdfTK = require('./pdftk/index');
const ErrorHandler = require('./errors_handler/index');

const PLUGINS = [
  PdfTK,
  ErrorHandler,
];

module.exports = PLUGINS.concat(API);
