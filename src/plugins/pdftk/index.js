const fs = require('fs');
const path = require('path');
const config = require('./config.json');
const { exec } = require('child_process');
const { addPdf, setPdf, getChain } = require('./handlePdf.js');

const pathToFile = path
  .join(path
  .dirname(require.main.filename), `../${config.tempfolder}`);

module.exports = ({ ACTIONS, utils }) => {

  ACTIONS.on('pdftk.create', (options) => {

    return new Promise((resolve, reject) => {

      const pdfName = addPdf({ length: options.length });

      fs.mkdir(pathToFile + '/' + pdfName);
      fs.writeFile(`${pathToFile}/${pdfName}/${config.s}`, options.file,
        (err) => {

        (!err) ? resolve(pdfName) : reject({ code: 104, message: err.message });

      });

    });

  });

  ACTIONS.on('pdftk.handle', ({ id, pages }) => {

    return new Promise((resolve, reject) => {

      console.log('preset', id, pages);

      setPdf(id, pages);

      const pdfOptions = getChain(id);

      console.log('ddd', pdfOptions);

      exec(`pdftk ${
        pathToFile}/${
          id}/${
          config.s} cat ${
            pdfOptions} output ${
              pathToFile}/${
                id}/${
                  config.e}`, (error, stdout, stderr) => {

                    if (!error) {

                        resolve(`${pathToFile}/${id}/${config.e}`);
                        ACTIONS.send('pdftk.clear', { id });

                    } else reject({ code: 107, message: error.message });

      });

    });

  });

  ACTIONS.on('pdftk.clear', ({ id }) => {

    return new Promise((resolve, reject) => {

      setTimeout(() => {

        fs.unlink(`${pathToFile}/${id}/${config.s}`, (err) => {

          fs.rmdir(pathToFile + '/' + id);

        });

      }, config.cleartime);

    });

  });

};
