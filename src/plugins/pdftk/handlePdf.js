const uuid = require('uuid');
const R = require('ramda');

// Registry of pdf connections
const connections = {};

module.exports = {

  addPdf: ({ length = 0, pages = [] }) => {

    const filename = uuid();

    connections[filename] = { length, pages: [] };

    return filename;

  },

  setPdf: (id, pages) => {

    if (connections[id]) {

      connections[id] = Object.assign({}, {
        length: connections[id].length,
        pages,
      });

      return connections[id];

    } else {

      throw new Error(`pdf file ${id} not found!`);

    }

  },

  getChain: (id) => {

    if (connections[id]) {

      const numSort = R.sortWith([
        R.ascend(R.prop('num')),
      ]);

      if (typeof(connections[id].pages) === 'string') {

        connections[id].pages = JSON.parse(connections[id].pages);

      }

      const sorted = numSort(connections[id].pages);
      let temp = 0;

      return sorted.map((page, index) => {

        let old = temp;
        temp = page.num + 1;
        const deleteResult = old + '-' + (page.num -1);
        const rotateResult = deleteResult + ' ' + page.num + page.value;

        if (index === 0 && page.type === 'rotate') {

          return '1' + page.value;

        } else if (index >= 0 && page.type === 'delete') {

          if (index === sorted.length) temp = 0;
          return deleteResult;

        } else if (index > 0 && page.type === 'rotate') {

          if (index === sorted.length) temp = 0;
          return rotateResult;

        }

      }).join(' ');

    } else {

      throw new Error(`pdf file ${id} not found!`);

    }

  },

};
