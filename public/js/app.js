/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "./";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(0);

/********** Paste your code here! ************/

var APP = {

  pageOne: false,
  pageTwo: false,
  contextOne: false,
  contextTwo: false,
  currentPdf: false,
  temp: 0,
  link: '/files/download?id=',
  //85679c30-7790-4e80-b046-edbff70f8494&pages=[{"num":123,"type":"delete"}]

  pages: {
    1: 0,
  },

  pagesSend: [],

  init: function() {
    document.getElementById('file-input')
      .addEventListener('change', this.readSingleFile, false);
    document.getElementById('save')
      .addEventListener('click', this.save);
    document.getElementById('addpage')
      .addEventListener('click', this.addPage);

    this.getList();
    this.initCanvas();
  },

  initCanvas: function() {
    this.pageOne = document.getElementById('page-1');
    this.contextOne = this.pageOne.getContext('2d');
  },

  getList: function() {
    document.getElementById('list').innerHTML = '';
    this.pagesSend.forEach((page) => {
      document.getElementById('list').innerHTML += '<li>Page: '+page.num+' type: '+page.type+' '+page.value+'</li>';
    });
  },

  readSingleFile: function(e) {

    var file = e.target.files[0];
    if (!file) return false;

    var reader = new FileReader();
    reader.onload = function(e) {
      var contents = e.target.result;
      console.log(e.target);
      var myData = new Uint8Array(e.target.result)

      PDFJS.getDocument({ data: myData }).then(APP.getDocument);

    };
    reader.readAsArrayBuffer(file);

  },

  getDocument: function(pdf) {

    APP.currentPdf = pdf;

    document.getElementById('number').max = pdf.pdfInfo.numPages;
    document.querySelector('.output').style.display = 'block';

    pdf.getPage(1).then(function(page) {

      var scale = 0.2;
      var viewport = page.getViewport(scale);
      var renderContext = {
        canvasContext: APP.contextOne,
        viewport: viewport
      };
      var renderTask = page.render(renderContext);
      renderTask.then(function () {
        // console.log('Page rendered');
      });

    });

  },

  addPage() {
    var number = document.getElementById('number');
    var type = document.getElementById('type');
    var value = document.getElementById('value');

    var finded = APP.pagesSend.find(function (pages) {
      return pages.num === parseInt(number.value);
    });

    if (!finded) {

      APP.pagesSend.push({
        num: parseInt(number.value),
        type: type.value,
        value: value.value,
      });

      APP.save();

    }

    APP.getList();
  },

  save() {
    var link = document.getElementById('link');
    var handled = APP.pagesSend.map(function (page) {
      if (page.value === '90') {page.value = 'east'};
      if (page.value === '180') {page.value = 'south'};
      if (page.value === '270') {page.value = 'west'};
      return page;
    });

    link.href = APP.link + window.fileID + '&pages=' + JSON.stringify(APP.pagesSend);
  },

};

document.getElementById('file-input').addEventListener('change', function() {
    let files = event.target.files;

    if(files.length > 0) {
        let formData = new FormData();

        formData.append('uploads', files[0]);

        fetch('/files/upload', {
            method: 'POST',
            body: formData
        })
        .then(res => res.json().then(data => { window.fileID = data.fileId }));
    }
});

document.getElementById('number').oninput = function valid() {
  var value = parseInt(this.value);
  var min = parseInt(this.min);
  var max = parseInt(this.max);
  if (value > max) value = max;
  if (value < min) value = min;
}

APP.init();

/***/ })
/******/ ]);
//# sourceMappingURL=app.js.map