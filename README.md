# RotatePDF
Manipulate pdf files service.

## Quick start

```bash
git clone https://gitlab.com/Dugnist/rotatePDF.git
npm i
npm start
```

## API

- `files/upload` - upload pdf file
- `files/download` - rotate and download

## Author

**Dugnist Alexey**

- <http://github.com/Dugnist>
- <https://www.facebook.com/Dugnist>



## Copyright and license

Code and documentation copyright 2017 JsBerry. Code released under [the MIT license](LICENSE).
